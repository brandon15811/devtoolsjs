# Setup
- Get php source
    - Apply patches to source
    - Initialize git submodules for `phpyaml` extension
- Get `libyaml` source
    - Compile with emscripten
- Used docker container: `apiaryio/emcc:1.37`
