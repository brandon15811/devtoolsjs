# CC = emcc
PREFIX=/emcc/php-src-emcc/install
CFLAGS = -O2 -c -I$(PREFIX)/include/php/ \
			-I$(PREFIX)/include/php/main \
			-I$(PREFIX)/include/php/Zend \
			-I$(PREFIX)/include/php/TSRM \
			-Wall -g


EXPORTED_FUNCTIONS = "['_main', '_eval', '_buildPhar', '_unzipCode']"
PRE_JS_FILE = /emcc/phpembed-emcc/prejs.js
CONSOLE_SCRIPT_FILE = /emcc/phpembed-emcc/ConsoleScript.php@/emcc/phpembed-emcc/ConsoleScript.php
INI_FILE = /emcc/phpembed-emcc/embed4.ini@/emcc/phpembed-emcc/embed4.ini
EM_SHELL_FILE = /emcc/phpembed-emcc/shell-material.html
EMSCRIPTEN_FLAGS = -s USE_ZLIB=1 -s EXPORTED_FUNCTIONS=$(EXPORTED_FUNCTIONS) -s TOTAL_MEMORY=33554432 --pre-js $(PRE_JS_FILE) --embed-file $(CONSOLE_SCRIPT_FILE) --embed-file $(INI_FILE) --shell-file $(EM_SHELL_FILE) --minify 0

OUTPUT_PREFIX=php-embed
OUTPUT_DIR=/emcc/web
CFILE = embed4

LDFLAGS = -O2 -L$(PREFIX)/lib $(PREFIX)/../libphp7.la 

LIBTOOL = $(PREFIX)/../libtool

all: $(CFILE).c
	$(CC) -o $(OUTPUT_PREFIX).o $(CFILE).c $(CFLAGS)
	$(LIBTOOL) --mode=link $(CC) -o $(OUTPUT_PREFIX).html $(LDFLAGS) $(EMSCRIPTEN_FLAGS) $(OUTPUT_PREFIX).o

.PHONY: install

install: all
	rm -f $(OUTPUT_DIR)/$(OUTPUT_PREFIX).*
	install $(OUTPUT_PREFIX).* $(OUTPUT_DIR)
#   Add .js so surge will gzip this file
	mv $(OUTPUT_DIR)/$(OUTPUT_PREFIX).html.mem $(OUTPUT_DIR)/$(OUTPUT_PREFIX).html.mem.js

.PHONY: clean

clean:
	rm -f $(OUTPUT_PREFIX).*

