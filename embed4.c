#include <sapi/embed/php_embed.h>
#include <ext/standard/info.h>
#include <emscripten.h>

//Based off of code from the book "Extending and Embedding PHP": Chapter 19 & 20

int callFunc(char *funcName, char *params[], int paramLength, zval *result);
int execute_php(char *filename);
int eval(char *code);
int buildPhar(char *make, char *relative, char *out, char *entry, char *stub);
void getType(zval *localRetval);

void getType(zval *localRetval)
{
    switch (Z_TYPE_P(localRetval))
    {
    case IS_NULL:
        php_printf("NULL: null\n");
        break;
    case IS_TRUE:
        php_printf("BOOL: false\n");
        break;
    case IS_FALSE:
        php_printf("BOOL: false\n");
        break;
    case IS_LONG:
        php_printf("LONG: %ld\n", Z_LVAL_P(localRetval));
        break;
    case IS_DOUBLE:
        php_printf("DOUBLE: %g\n", Z_DVAL_P(localRetval));
        break;
    case IS_STRING:
        php_printf("STRING: value=\"");
        PHPWRITE(Z_STRVAL_P(localRetval), Z_STRLEN_P(localRetval));
        php_printf("\", length=%d\n", Z_STRLEN_P(localRetval));
        break;
    case IS_RESOURCE:
        // php_printf("RESOURCE: id=%ld\n", Z_RESVAL(localRetval));
        php_printf("%s\n", "RESOURCE");
        break;
    case IS_ARRAY:
        php_printf("ARRAY: hashtable=%p\n", Z_ARRVAL_P(localRetval));
        break;
    case IS_OBJECT:
        php_printf("OBJECT: ???\n");
        break;
    }
}

int callFunc(char *funcName, char *params[], int paramLength, zval *result)
{
    int status;
    zval *args[paramLength];
    zval localRetval, arg[paramLength], zendFuncName;

    ZVAL_STRING(&zendFuncName, funcName);
    int x;
    for (x = 0; x < paramLength; x++)
    {
        //Initialize args[x] with a zval
        args[x] = &arg[x];
        if (params[x] == NULL)
        {
            ZVAL_NULL(args[x]);
        }
        else
        {
            ZVAL_STRINGL(args[x], params[x], strlen(params[x]));
        }
    }
    status = call_user_function_ex(EG(function_table), NULL,
                                   &zendFuncName, &localRetval, paramLength, (*args), 0, NULL TSRMLS_CC);
    printf("callFunc es: %d, EG: %d\n", status, EG(exit_status));
    zval_ptr_dtor(&zendFuncName);
    for (x = 0; x < paramLength; x++)
    {
        zval_ptr_dtor(args[x]);
    }
    ZVAL_COPY_VALUE(result, &localRetval);

    return status;
}

int require_script(char *filename)
{
    int ret;
    int status;
    zend_first_try
    {
        char *include_script;
        spprintf(&include_script, 0, "require '%s';", filename);
        ret = zend_eval_string_ex(include_script, NULL, filename TSRMLS_CC, 1);
        efree(include_script);
    }
    zend_end_try();
    if (ret != 0)
    {
        status = EG(exit_status);
    }
    else
    {
        status = ret;
    }
    printf("%s ret: %d, es: %d, EG: %d\n", filename, ret, status, EG(exit_status));
    return status;
}

int eval(char *code)
{
    int status;
    zend_first_try
    {
        status = zend_eval_string_ex(code, NULL, "phpEval" TSRMLS_CC, 1);
    }
    zend_catch
    {
        status = -1;
        printf("eval failed\n");
    }
    zend_end_try();
    return status;
}

int buildPhar(char *make, char *relative, char *out, char *entry, char *stub)
{
    int ret;
    int status;

    zval result;
    zend_first_try
    {
        char *funcParam[5] = {make, relative, out, entry, stub};
        ret = callFunc("buildPhar", funcParam, 5, &result);
    }
    zend_catch
    {
        printf("An error occurred while building the phar\n");
    }
    zend_end_try();
    if (ret != 0)
    {
        status = EG(exit_status);
    }
    else
    {
        status = Z_LVAL(result);
    }
    printf("buildPhar ret: %d, es: %d, EG: %d\n", ret, status, EG(exit_status));
    return status;
}

int unzipCode()
{
    int ret;
    int status;

    zval result;
    zend_first_try
    {
        char *funcParam[1] = {NULL};
        ret = callFunc("unzipCode", funcParam, 1, &result);
    }
    zend_catch
    {
        printf("An error occurred while unzipping the code\n");
    }
    zend_end_try();
    if (ret != 0)
    {
        status = EG(exit_status);
    }
    else
    {
        status = Z_LVAL(result);
    }
    printf("unzipCode ret: %d, es: %d, EG: %d\n", ret, status, EG(exit_status));
    return status;
}

// static int (*original_embed_startup)(struct _sapi_module_struct *sapi_module);

/*
int embed4_startup_callback(struct _sapi_module_struct *sapi_module)
{
    // Call original startup callback first,
    // otherwise the environment won't be ready
    if (original_embed_startup(sapi_module) == FAILURE)
    {
        //Application failure handling may occur here
        return FAILURE;
    }
    // Calling the original embed_startup actually places us
    // in the ACTIVATE stage rather than the STARTUP stage, but
    // we can still alter most INI_SYSTEM and INI_PERDIR entries anyhow

    zval iniName, iniValue;
    ZVAL_STRING(&iniName, "phar.readonly");
    ZVAL_STRING(&iniValue, "0");
    int status = zend_alter_ini_entry_ex(Z_STR(iniName), Z_STR(iniValue), PHP_INI_SYSTEM, PHP_INI_STAGE_ACTIVATE, 1);
    printf("ini retval: %d\n", status);

    return SUCCESS;
}
*/

int main(int argc, char *argv[])
{
    php_embed_module.phpinfo_as_text = 1;
    php_embed_module.php_ini_path_override = "/emcc/phpembed-emcc/embed4.ini";

    php_embed_init(argc, argv);
    require_script("/emcc/phpembed-emcc/ConsoleScript.php");

    EM_ASM(
        Module['onPHPInitialized']();
    );

    return 0;
}