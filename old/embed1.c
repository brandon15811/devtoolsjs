#include <stdio.h>
#include <sapi/embed/php_embed.h>
int main(int argc, char *argv[])
{
    zend_file_handle script;
    /* Basic parameter checking */
    if (argc <= 1)
    {
        fprintf(stderr, "Usage: embed1 filename.php <arguments>\n");
        return -1;
    }
    /* Set up a File Handle structure */
    script.type = ZEND_HANDLE_FP;
    script.filename = argv[1];
    script.opened_path = NULL;
    script.free_filename = 0;
    if (!(script.handle.fp = fopen(script.filename, "rb")))
    {
        fprintf(stderr, "Unable to open: %s\n", argv[1]);
        return -1;
    }
    /* Ignore argv[0] when passing to PHP */
    argc;
    argv++;
    printf("hi");
    PHP_EMBED_START_BLOCK(argc, argv)
    php_execute_script(&script TSRMLS_CC);
    printf("bye");
    PHP_EMBED_END_BLOCK()
    return 0;
}