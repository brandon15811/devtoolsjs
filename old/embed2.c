#include <sapi/embed/php_embed.h>
int main(int argc, char *argv[])
{
    PHP_EMBED_START_BLOCK(argc, argv)
    zend_eval_string("echo 'Hello World!';", NULL,
                     "Simple Hello World App" TSRMLS_CC);
    PHP_EMBED_END_BLOCK()
    return 0;
}