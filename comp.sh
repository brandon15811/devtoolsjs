#!/bin/bash -ex

#TODO:
#   Fix date extension

#emconfigure ./configure  --disable-cgi --disable-short-tags --disable-ipv6 --disable-all --disable-libxml \
#	--disable-ctype --disable-inifile --disable-flatfile --disable-dom --disable-fileinfo --disable-filter \
#	--disable-hash --disable-json --disable-mbregex --disable-mbregex-backtrack --disable-opcache \
#	--disable-opcache-file --disable-huge-code-pages --disable-pdo --disable-posix --disable-session \
#	--disable-simplexml --disable-tokenizer --disable-xml --disable-xmlreader --disable-xmlwriter \
#	--disable-mysqlnd-compression-support --without-pcre-regex --without-pcre-jit --enable-phar CFLAGS=-O2 CXXFLAGS=-O2


#./buildconf --force

case "$1" in
buildconf)
    echo "Running buildconf"
    # make distclean
    ./buildconf --force
    ;&
configure)
    echo "Running configure"
    emconfigure ./configure  --disable-cgi --disable-short-tags --disable-ipv6 --disable-all --disable-libxml --disable-ctype --disable-inifile --disable-flatfile --disable-dom --disable-fileinfo --disable-filter --disable-hash --disable-mbregex --disable-mbregex-backtrack --disable-opcache --disable-opcache-file --disable-huge-code-pages --disable-pdo --disable-posix --disable-session --disable-simplexml --disable-xml --disable-xmlreader --disable-xmlwriter --disable-mysqlnd-compression-support --disable-phpdbg --disable-tokenizer --disable-json --disable-pcntl --without-pcre-regex --without-pcre-jit --enable-phar --enable-cli --enable-zip --enable-embed=static --prefix="${PWD}/install/" --with-yaml="/emcc/yaml-0.1.7-emcc/install" --host=x86_64-unknown-linux-gnu CFLAGS="-fdata-sections -ffunction-sections -s USE_ZLIB=1 -O2" CXXFLAGS="-fdata-sections -ffunction-sections -O2" LDFLAGS="-Wl,--gc-sections -Wl,-s"
    #Needed for psysh: --enable-tokenizer --enable-json

    sed -i 's|#define HAVE_OLD_READDIR_R 1|/* #undef HAVE_OLD_READDIR_R */|' main/php_config.h
    sed -i 's|/\* #undef HAVE_UTIME \*/|#define HAVE_UTIME 1|' main/php_config.h
    ;&
make)
    echo "Running make"
    emmake make clean
    emmake make -j8
    ;&
install)
    echo "Running install"
    # ln -s $PWD/libs/libphp7.{a,so}
    emmake make install
    ;&
emcc)
    echo "Running emcc"
    EMBED_PREFIX=/emcc/phpembed-emcc/
    cd $EMBED_PREFIX
    emmake make
    emmake install
    ;;
*)
    echo "No option specified"
    exit 1
    ;&
esac



#./buildconf --force && ./configure  --disable-cgi --disable-short-tags --disable-ipv6 --disable-all --disable-libxml --disable-ctype --disable-inifile --disable-flatfile --disable-dom --disable-fileinfo --disable-filter --disable-hash --disable-json --disable-mbregex --disable-mbregex-backtrack --disable-opcache --disable-opcache-file --disable-huge-code-pages --disable-pdo --disable-posix --disable-session --disable-simplexml --disable-tokenizer --disable-xml --disable-xmlreader --disable-xmlwriter --disable-mysqlnd-compression-support --disable-phpdbg --without-pcre-regex --without-pcre-jit --disable-phar --enable-cli --enable-zip CFLAGS="-O2 -fdata-sections -ffunction-sections" CXXFLAGS="-O2 -fdata-sections -ffunction-sections" LDFLAGS="-Wl,--gc-sections" && make clean && make -j8
