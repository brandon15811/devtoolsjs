//From emscripten
var ENVIRONMENT_IS_WEB = false;
var ENVIRONMENT_IS_WORKER = false;
var ENVIRONMENT_IS_NODE = false;
var ENVIRONMENT_IS_SHELL = false;

ENVIRONMENT_IS_WEB = typeof window === 'object';
ENVIRONMENT_IS_WORKER = typeof importScripts === 'function';
ENVIRONMENT_IS_NODE = typeof process === 'object' && typeof require === 'function' && !ENVIRONMENT_IS_WEB && !ENVIRONMENT_IS_WORKER;
ENVIRONMENT_IS_SHELL = !ENVIRONMENT_IS_WEB && !ENVIRONMENT_IS_NODE && !ENVIRONMENT_IS_WORKER

var buildPhar, phpEval, unzipCode;


// importScripts('https://unpkg.com/promise-worker/dist/promise-worker.register.js');
if (typeof Module === 'undefined') {
    var Module = {
        'preRun': [],
    };
}
function mountWorking() {
    FS.mkdir('/working');
    if (ENVIRONMENT_IS_NODE) {
        FS.mount(NODEFS, { root: '.' }, '/working');
        FS.mount(NODEFS, { root: '/emcc' }, '/emcc');
    } else {
        FS.mount(MEMFS, '/working');
    }
}
function exposeFunctions() {
    buildPhar = Module.cwrap('buildPhar', 'number', ['string', 'string', 'string', 'string', 'string']);
    phpEval = Module.cwrap('eval', 'number', ['string']);
    unzipCode = Module.cwrap('unzipCode', 'number', []);
}
Module['preRun'].push(mountWorking);
Module['preRun'].push(exposeFunctions);

// Module['onCustomMessage'] = function (message) {
//     if (message['method'] === 'writeFile') {
//         var params = message['params'];
//         FS.writeFile(params['name'], params['data'], params['options']);
//     }
// }

// registerPromiseWorker(function (message) {
//     if (message['method'] === 'writeFile') {
//         var params = message['params'];
//         FS.writeFile(params['name'], params['data'], params['options']);
//     }
// });
// postMessage({'target': 'custom', 'event': 'ready'});
if (!String.prototype.endsWith) {
    String.prototype.endsWith = function (searchString, position) {
        var subjectString = this.toString();
        if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
            position = subjectString.length;
        }
        position -= searchString.length;
        var lastIndex = subjectString.lastIndexOf(searchString, position);
        return lastIndex !== -1 && lastIndex === position;
    };
}



//Module['arguments'] = ['-dphar.readonly=0', '/files/unzip.php', '--make=/output/', '--relative=/output/', '--out', '/output/plugin.phar'];
// Module['arguments'] = [
//     '-dphar.readonly=0',
//     '/emcc/ConsoleScript.php',
//     // '/emcc/unzip.php',
//     '--make', '/working/plugin',
//     '--relative', '/working/plugin',
//     '--out', '/working/plugin.phar'
// ];

// if (ENVIRONMENT_IS_NODE && process.env['USEARGS'] !== '1') {
//     delete Module['arguments'];
// }
